import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import threadReducer from './containers/Thread/reducer';
import profileReducer from './containers/Profile/reducer';

const initialState = {};

const middlewares = [ thunk ];

const composedEnhancers = composeWithDevTools(
  applyMiddleware(...middlewares)
);

const reducers = {
  posts: threadReducer,
  profile: profileReducer
};

const rootReducer = combineReducers(reducers);

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
);

export default store;
