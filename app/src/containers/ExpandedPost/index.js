import React, { useState, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { THREAD } from '../../routes';
import {
  Container, Header, Footer, FooterTab,
  Body, Left, Right, Title, Button, Icon, Text
} from 'native-base';
import {
  setPostReaction,
  setCommentReaction,
  toggleExpandedPost,
  addComment,
  updatePost,
  updateComment,
  deletePost,
  deleteComment
} from '../Thread/actions';
import * as imageService from '../../services/imageService';
import Post from '../../components/Post';
import Comment from '../../components/Comment';
import globalStyles from '../../styles';
import SplashScreen from '../SplashScreen';
import EditCommentModal from '../Modals/EditCommentModal';
import CommentMenuModal from '../Modals/CommentMenuModal';
import EditPostModal from '../Modals/EditPostModal';
import PostMenuModal from '../Modals/PostMenuModal';
import { findById, createAlert } from '../../helpers/common';

const ExpandedPost = ({
  posts,
  post,
  user,
  setPostReaction: postReaction,
  setCommentReaction: commentReaction,
  toggleExpandedPost: toggle,
  addComment: add,
  updatePost: updPost,
  updateComment: update,
  deletePost: delPost,
  deleteComment: delComment
}) => {
  const [isCommentFormOpened, setIsCommentFormOpened] = useState(false);
  const [isCommentMenuOpened, setIsCommentMenuOpened] = useState(false);
  const [isPostFormOpened, setIsPostFormOpened] = useState(false);
  const [isPostMenuOpened, setIsPostMenuOpened] = useState(false);
  const [commentInAction, setCommentInAction] = useState({});
  const [postInAction, setPostInAction] = useState({});
  const navigation = useNavigation();
  const commentsRef = useRef(null);

  const scrollToBottom = useCallback(() => {
    commentsRef.current.scrollToEnd();
  }, [commentsRef]);

  const backToThread = useCallback(() => {
    toggle();
    navigation.goBack();
  }, [toggle]);

  const closeModal = useCallback(() => {
    setIsCommentFormOpened(false);
    setIsCommentMenuOpened(false);
    setIsPostMenuOpened(false);
    setIsPostFormOpened(false);
  }, []);

  const openCommentMenu = comment => {
    setCommentInAction(comment);
    setIsCommentMenuOpened(true);
  };

  const openPostMenu = post => {
    setPostInAction(post);
    setIsPostMenuOpened(true);
  };

  const uploadImage = file => imageService.uploadImage(file);

  const applyComment = async comment => {
    comment.id
      ? await update(comment)
      : await add({ postId: post.id, body: comment.body });
      
      scrollToBottom();
  };

  const onDeletePost = postId => {
    createAlert(
      'Last chance...',
      'Are you sure, you want delete this post?',
      () => {
        delPost(postId);
        navigation.navigate(THREAD);
      }
    );
    closeModal();
    setPostInAction({});
  };

  const onDeleteComment = commentId => {
    createAlert(
      'Last chance...',
      'Are you sure, you want delete this comment?',
      () => delComment(commentId)
    );
    closeModal();
    setCommentInAction({});
  };

  const handlePostForm = (postId = '') => {
    const post = postId ? findById(postId, posts) : {};
    setPostInAction(post);
    if (isPostMenuOpened) {
      setIsPostMenuOpened(false);
    }
    setIsPostFormOpened(true);
  };

  const handleCommentForm = (commentId = '') => {
    const comment = commentId ? findById(commentId, post.comments) : {};
    setCommentInAction(comment);
    if (isCommentMenuOpened) {
      setIsCommentMenuOpened(false);
    }
    setIsCommentFormOpened(true);
  };

  const renderComment = useCallback(({ item }) => (
    <Comment
      entity={item}
      currentUserId={user.id}
      setReaction={commentReaction}
      editEntity={handleCommentForm}
      deleteEntity={delComment}
      openMenu={openCommentMenu}
      toggleExpandedPost={() => { }}
    />
  ), []);

  const keyExtractor = useCallback(item => item.id, []);

  const renderPost = () => (
    <Post
      entity={post}
      currentUserId={user.id}
      setReaction={postReaction}
      editEntity={() => { }}
      deleteEntity={delPost}
      openMenu={openPostMenu}
      toggleExpandedPost={() => { }}
    />
  );

  return (
    <Container style={globalStyles.container}>
      <Header>
        <Left>
          <Button transparent onPress={backToThread}>
            <Icon name='arrow-back' />
          </Button>
        </Left>
        <Body>
          <Title>Post</Title>
        </Body>
        <Right />
      </Header>

      {
        post.id ? (
          post.comments ? (
            <FlatList
              ref={commentsRef}
              ListHeaderComponent={renderPost}
              data={post.comments}
              renderItem={renderComment}
              keyExtractor={keyExtractor}
            />
          ) : (renderPost())
        ) : <SplashScreen />
      }

      <Footer>
        <FooterTab>
          <Button style={globalStyles.commentButton} onPress={handleCommentForm}>
            <Icon name='chatbubble-ellipses' />
            <Text style={globalStyles.fs16}>Add comment</Text>
          </Button>
        </FooterTab>
      </Footer>

      {
        isCommentFormOpened &&
        <EditCommentModal
          comment={commentInAction}
          isOpened={isCommentFormOpened}
          apply={applyComment}
          close={closeModal}
        />
      }

      {
        isPostFormOpened &&
        <EditPostModal
          post={postInAction}
          isOpened={isPostFormOpened}
          apply={updPost}
          uploadImage={uploadImage}
          close={closeModal}
        />
      }

      {
        isPostMenuOpened &&
        <PostMenuModal
          postId={postInAction.id}
          isOpened={isPostMenuOpened}
          close={closeModal}
          del={onDeletePost}
          edit={handlePostForm}
        />
      }

      {
        isCommentMenuOpened &&
        <CommentMenuModal
          commentId={commentInAction.id}
          isOpened={isCommentMenuOpened}
          close={closeModal}
          del={onDeleteComment}
          edit={handleCommentForm}
        />
      }
    </Container>
  );
};

ExpandedPost.defaultProps = {
  post: {}
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  setPostReaction: PropTypes.func.isRequired,
  setCommentReaction: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  post: rootState.posts.expandedPost,
  user: rootState.profile.user
});

const actions = {
  setPostReaction,
  setCommentReaction,
  toggleExpandedPost,
  addComment,
  updatePost,
  updateComment,
  deletePost,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedPost);
