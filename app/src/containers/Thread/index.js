import React, { useState, useEffect, useCallback, useRef } from 'react';
import { useNavigation } from '@react-navigation/native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from '../../services/imageService';
import { POST } from '../../routes';
import { FlatList } from 'react-native';
import { Container } from 'native-base';
import Post from '../../components/Post';
import SplashScreen from '../SplashScreen';
import ThreadHeader from './ThreadHeader';
import EditPostModal from '../Modals/EditPostModal';
import PostMenuModal from '../Modals/PostMenuModal';
import SortMenuModal from '../Modals/SortMenuModal';
import { findById, createAlert } from '../../helpers/common';
import {
  loadPosts,
  loadMorePosts,
  toggleExpandedPost,
  setPostReaction,
  addPost,
  updatePost,
  deletePost
} from './actions';
import { logout } from '../Profile/actions';

const postsFilter = {
  not: undefined,
  userId: undefined,
  from: 0,
  count: 10,
  column: 'createdAt',
  desc: 1
};

const Thread = ({
  user,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  hasMorePosts,
  addPost: createPost,
  updatePost: update,
  setPostReaction: postReaction,
  toggleExpandedPost: setExpandedPost,
  deletePost: del,
  logout: signOut
}) => {
  const navigation = useNavigation();
  const [isPostFormOpened, setIsPostFormOpened] = useState(false);
  const [isPostMenuOpened, setIsPostMenuOpened] = useState(false);
  const [isSortMenuOpened, setIsSortMenuOpened] = useState(false);
  const [postInAction, setPostInAction] = useState({});
  const [showOwnPosts, setShowOwnPosts] = useState(false);

  const threadListRef = useRef(null);

  useEffect(() => {
    load(postsFilter);
  }, [postsFilter]);

  const scrollToTop = useCallback(() => {
    threadListRef.current.scrollToOffset({ offset: 0 });
  }, []);

  const closeModal = useCallback(() => {
    setIsPostFormOpened(false);
    setIsPostMenuOpened(false);
    setIsSortMenuOpened(false);
  }, []);

  const openExpandedPost = postId => {
    setExpandedPost(postId);
    navigation.navigate(POST);
  };

  const openPostMenu = post => {
    setPostInAction(post);
    setIsPostMenuOpened(true);
  };

  const uploadImage = file => imageService.uploadImage(file);

  const applyPost = async post => {
    post.id
      ? await update({ id: post.id, body: post.body })
      : await createPost(post)
  };

  const onDeletePost = postId => {
    createAlert(
      'Last chance...',
      'Are you sure, you want delete this post?',
      () => del(postId)
    );
    closeModal();
    setPostInAction({});
  };

  const handlePostForm = (postId = '') => {
    const post = postId ? findById(postId, posts) : {};
    setPostInAction(post);
    if (isPostMenuOpened) {
      setIsPostMenuOpened(false);
    }
    setIsPostFormOpened(true);
  };

  const getMorePosts = () => {
    if (!hasMorePosts) {
      return;
    }

    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const handleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : user.id;
    postsFilter.from = 0;
    load(postsFilter);
    scrollToTop();
    // postsFilter.from = postsFilter.count; // for the next scroll
  };

  const openSortMenu = useCallback(() => {
    setIsSortMenuOpened(true);
  }, []);

  const sortBy = column => {
    const order = postsFilter.column === column
      ? 1 - postsFilter.desc
      : 1;
    postsFilter.from = 0;
    postsFilter.column = column;
    postsFilter.desc = order;
    closeModal();
    load(postsFilter);
    scrollToTop();
  };

  const sortByDate = useCallback(() => sortBy('createdAt'), []);
  const sortByLikes = useCallback(() => sortBy('likeCount'), []);
  const sortByDislikes = useCallback(() => sortBy('dislikeCount'), []);
  const sortByComments = useCallback(() => sortBy('commentCount'), []);

  const renderPost = useCallback(({ item }) => (
    <Post
      entity={item}
      currentUserId={user.id}
      setReaction={postReaction}
      editEntity={handlePostForm}
      deleteEntity={del}
      openMenu={openPostMenu}
      toggleExpandedPost={openExpandedPost}
    />
  ), []);

  const keyExtractor = useCallback(item => item.id, []);

  return (
    <Container>
      <ThreadHeader
        user={user}
        handleSort={openSortMenu}
        showOwnPosts={showOwnPosts}
        handleShowOwnPosts={handleShowOwnPosts}
        handleNewPost={handlePostForm}
        signOut={signOut}
      />

      <FlatList
        ref={threadListRef}
        data={posts}
        keyExtractor={keyExtractor}
        initialNumToRender={10}
        onEndReached={getMorePosts}
        onEndReachedThreshold={0.8}
        renderItem={renderPost}
        ListEmptyComponent={<SplashScreen />}
      />

      {
        isPostFormOpened &&
        <EditPostModal
          post={postInAction}
          isOpened={isPostFormOpened}
          apply={applyPost}
          uploadImage={uploadImage}
          close={closeModal}
        />
      }

      {
        isPostMenuOpened &&
        <PostMenuModal
          postId={postInAction.id}
          isOpened={isPostMenuOpened}
          close={closeModal}
          del={onDeletePost}
          edit={handlePostForm}
        />
      }

      {
        isSortMenuOpened &&
        <SortMenuModal
          isOpened={isSortMenuOpened}
          close={closeModal}
          byDate={sortByDate}
          byLikes={sortByLikes}
          byDislikes={sortByDislikes}
          byComments={sortByComments}
        />
      }

    </Container>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  user: PropTypes.objectOf(PropTypes.any),
  hasMorePosts: PropTypes.bool,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  setPostReaction: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  user: {}
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  user: rootState.profile.user
});

const actions = {
  loadPosts,
  loadMorePosts,
  setPostReaction,
  toggleExpandedPost,
  addPost,
  updatePost,
  deletePost,
  logout
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Thread);
