import * as postService from '../../services/postService';
import * as commentService from '../../services/commentService';
import { findIdxById } from '../../helpers/common';
import {
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

const deletePostAction = post => ({
  type: DELETE_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  try {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
  } catch (error) {
    console.log(error);
  }
};

export const updatePost = post => async (dispatch, getRootState) => {
  const { id } = await postService.updatePost(post);
  const updatedPost = await postService.getPost(id);
  const { posts: { expandedPost }} = getRootState();

  dispatch(updatePostAction(updatedPost));

  if (expandedPost && expandedPost.id === updatedPost.id) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const deletePost = postId => async dispatch => {
  const post = await postService.deletePost(postId);
  dispatch(deletePostAction(post));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const getPostReactions = postId => async (dispatch, getRootState) => {
  const reactions = await postService.getPostReactions(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const updatedPosts = posts.map(post => (post.id !== postId ? post : { ...post, reactions }));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction({ ...expandedPost, reactions }));
  }
};

export const getCommentReactions = commentId => async (dispatch, getRootState) => {
  const reactions = await commentService.getCommentReactions(commentId);
  const { posts: { expandedPost } } = getRootState();
  const comments = expandedPost.comments
    .map(comment => (comment.id !== commentId ? comment : { ...comment, reactions }));
  dispatch(setExpandedPostAction({ ...expandedPost, comments }));
};

export const setPostReaction = (postId, isLike) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.setPostReaction(postId, isLike);

  // if ID exists then the post was liked/disliked, otherwise - reaction was removed
  const diffPressed = id ? 1 : -1;
  // if pressed reaction was updated then remove ooposite reaction
  const diffOpposite = id && createdAt !== updatedAt ? -1 : 0;

  const mapReactions = post => {
    if (isLike) {
      return {
        ...post,
        likeCount: Number(post.likeCount) + diffPressed, // diffs are taken from the current closure
        dislikeCount: Number(post.dislikeCount) + diffOpposite
      };
    }
    return {
      ...post,
      likeCount: Number(post.likeCount) + diffOpposite,
      dislikeCount: Number(post.dislikeCount) + diffPressed
    };
  };

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReactions(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReactions(expandedPost)));
  }
};

export const setCommentReaction = (commentId, isLike) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.setCommentReaction(commentId, isLike);

  // if ID exists then the post was liked/disliked, otherwise - reaction was removed
  const diffPressed = id ? 1 : -1;
  // if pressed reaction was updated then remove ooposite reaction
  const diffOpposite = id && createdAt !== updatedAt ? -1 : 0;

  const mapCommentReactions = comment => {
    if (isLike) {
      return {
        ...comment,
        likeCount: Number(comment.likeCount) + diffPressed, // diffs are taken from the current closure
        dislikeCount: Number(comment.dislikeCount) + diffOpposite
      };
    }
    return {
      ...comment,
      likeCount: Number(comment.likeCount) + diffOpposite,
      dislikeCount: Number(comment.dislikeCount) + diffPressed
    };
  };

  const { posts: { expandedPost } } = getRootState();
  const updatedExpandedPost = {
    ...expandedPost,
    comments: expandedPost.comments.map(
      comment => (comment.id !== commentId ? comment : mapCommentReactions(comment))
    )
  };

  dispatch(setExpandedPostAction(updatedExpandedPost));
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.updateComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: post.comments.map(c => c.id === comment.id ? comment : c)
  });

  const { posts: { posts, expandedPost } } = getRootState();

  // Useless while we do not store comments in all posts list
  // const updated = posts.map(post => (post.id !== comment.postId
  //   ? post
  //   : mapComments(post)
  // ));

  // dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const deleteComment = id => async (dispatch, getRootState) => {
  const comment = await commentService.deleteComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...(post.comments || [])].filter((com => com.id !== comment.id))
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
