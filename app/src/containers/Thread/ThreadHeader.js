import React, { useCallback, useMemo } from 'react';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';
import { PROFILE } from '../../routes';
import { Switch } from 'react-native';
import { Header, Right, Left, Title, Body, Icon, Button, Thumbnail } from 'native-base';
import { createAlert, getUserImgLink } from '../../helpers/common';
import globalStyles from '../../styles';

const ThreadHeader = ({
  user,
  handleSort,
  showOwnPosts,
  handleShowOwnPosts,
  handleNewPost,
  signOut
}) => {
  const navigation = useNavigation();

  const avatarLink = useMemo(() => getUserImgLink(user), [user]);

  const handleProfileLink = useCallback(() => navigation.navigate(PROFILE), []);

  const handleLogOut = useCallback(() => createAlert(
    'Are you sure?',
    'Do you want to log out?',
    signOut
  ), [signOut]);

  return (
    <Header>
      <Left>
        <Button transparent onPress={handleProfileLink}>
          <Thumbnail small source={{ uri: avatarLink }} />
        </Button>
      </Left>

      <Body>
        <Title>{user.username}</Title>
      </Body>

      <Right style={globalStyles.alignCenter}>
        <Button style={globalStyles.mr10} transparent onPress={handleSort}>
          <Icon name='funnel' />
        </Button>
        <Switch
          onValueChange={handleShowOwnPosts}
          thumbColor={showOwnPosts ? "#E68A75" : "#f4f3f4"}
          value={showOwnPosts}
        />
        <Button transparent onPress={handleNewPost}>
          <Icon name='pencil' />
        </Button>
        <Button transparent onPress={handleLogOut}>
          <Icon name='log-out-outline' />
        </Button>
      </Right>
    </Header>
  );
};

ThreadHeader.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  handleSort: PropTypes.func.isRequired,
  showOwnPosts: PropTypes.bool.isRequired,
  handleShowOwnPosts: PropTypes.func.isRequired,
  handleNewPost: PropTypes.func.isRequired,
  signOut: PropTypes.func.isRequired,
}

export default ThreadHeader;
