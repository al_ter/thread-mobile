import React from "react";
import { StyleSheet, View } from "react-native";
import { Spinner } from 'native-base';

const SplashScreen = () => (
  <View style={[styles.container]}>
    <Spinner color='#555' />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  }
});

export default SplashScreen;
