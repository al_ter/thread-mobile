import React, { useState, useEffect } from 'react';
import { Textarea, Icon, Form, Button } from 'native-base';
import { View, Text } from 'react-native';
import Modal from '../../components/Modal';
import PropTypes from 'prop-types';
import globalStyles from '../../styles';

const EditCommentModal = ({ comment = {}, isOpened, apply, close }) => {
  const [body, setBody] = useState('');
  const [isUploading, setIsUploading] = useState(false);

  useEffect(() => (comment.id && setBody(comment.body)), [comment]);

  const handleAddComment = async () => {
    if (!body.trim()) {
      return;
    }
    setIsUploading(true);
    await apply({ ...comment, body });
    setIsUploading(false);
    setBody('');
    close();
  };

  return (
    <Modal isOpened={isOpened} close={close}>
      <Form>
        <Textarea
          value={body}
          rowSpan={5}
          bordered
          placeholder="So, what`s on your mind?"
          onChangeText={setBody}
        />
      </Form>
      <View style={globalStyles.formFooter}>
        <Button
          rounded
          iconRight
          disabled={isUploading}
          onPress={handleAddComment}
        >
          <Text style={globalStyles.formSendButtonText}>
            Send
          </Text>
          <Icon name='send' />
        </Button>
      </View>
    </Modal>
  );
};

EditCommentModal.defaultProps = {
  comment: {},
};

EditCommentModal.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any),
  isOpened: PropTypes.bool.isRequired,
  apply: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditCommentModal;
