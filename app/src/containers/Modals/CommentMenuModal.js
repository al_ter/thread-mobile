import React from 'react';
import { Button } from 'native-base';
import { Text } from 'react-native';
import Modal from '../../components/Modal';
import PropTypes from 'prop-types';
import globalStyles from '../../styles';

const CommentMenuModal = ({ commentId, isOpened, close, del, edit }) => (
  <Modal isOpened={isOpened} close={close} header='Comment actions'>
    <Button
      full
      transparent
      style={globalStyles.justifyFlexStart}
      onPress={() => edit(commentId)}
    >
      <Text style={globalStyles.fs18}>
        Edit comment
      </Text>
    </Button>
    <Button
      full
      transparent
      style={globalStyles.justifyFlexStart}
      onPress={() => del(commentId)}
    >
      <Text style={[globalStyles.fs18, globalStyles.colorRed]}>
        Delete comment
      </Text>
    </Button>
  </Modal>
);

CommentMenuModal.defaultProps = {
  commentId: ''
};

CommentMenuModal.propTypes = {
  commentId: PropTypes.string.isRequired,
  isOpened: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  del: PropTypes.func.isRequired,
  edit: PropTypes.func.isRequired
};

export default CommentMenuModal;
