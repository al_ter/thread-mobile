import React, { useState, useEffect } from 'react';
import { Textarea, Icon, Form, Button } from 'native-base';
import { View, Text } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import Modal from '../../components/Modal';
import PropTypes from 'prop-types';
import globalStyles from '../../styles';

const EditPostModal = ({ post = {}, isOpened, apply, uploadImage, close }) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  useEffect(() => (post.id && setBody(post.body)), [post]);

  const handleAddPost = async () => {
    if (!body.trim()) {
      return;
    }
    setIsUploading(true);
    await apply({
      ...post,
      body,
      imageId: image?.imageId
    });
    setIsUploading(false);
    setBody('');
    close();
  };

  const handleFilePicker = async () => {
    setIsUploading(true);
    try {
      const { uri, type, name } = await DocumentPicker.pick({
        type: [DocumentPicker.types.images]
      });

      const { id: imageId, link: imageLink } = await uploadImage({ uri, type, name });
      setImage({ imageId, imageLink });

    } catch (error) {
      console.log(error);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Modal isOpened={isOpened} close={close}>
      <Form>
        <Textarea
          value={body}
          rowSpan={5}
          bordered
          placeholder="So, what`s on your mind?"
          onChangeText={setBody}
        />
      </Form>
      <View style={globalStyles.formFooter}>
        <Button
          rounded
          iconRight
          disabled={isUploading}
          onPress={handleAddPost}
        >
          <Text style={globalStyles.formSendButtonText}>
            Send
          </Text>
          <Icon name='send' />
        </Button>
        <Button transparent onPress={handleFilePicker}>
          <Icon name='image' />
        </Button>
      </View>
    </Modal>
  );
};

EditPostModal.defaultProps = {
  post: {},
};

EditPostModal.propTypes = {
  post: PropTypes.objectOf(PropTypes.any),
  isOpened: PropTypes.bool.isRequired,
  apply: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditPostModal;
