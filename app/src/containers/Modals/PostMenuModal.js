import React from 'react';
import { Button } from 'native-base';
import { Text } from 'react-native';
import Modal from '../../components/Modal';
import PropTypes from 'prop-types';
import globalStyles from '../../styles';

const PostMenuModal = ({ postId, isOpened, close, del, edit }) => (
  <Modal isOpened={isOpened} close={close} header='Post actions'>
    <Button full transparent style={globalStyles.justifyFlexStart} onPress={() => edit(postId)}>
      <Text style={globalStyles.fs18}>
        Edit post
        </Text>
    </Button>
    <Button full transparent style={globalStyles.justifyFlexStart} onPress={() => del(postId)}>
      <Text style={[globalStyles.fs18, globalStyles.colorRed]}>
        Delete post
        </Text>
    </Button>
  </Modal>
);

PostMenuModal.defaultProps = {
  postId: ''
};

PostMenuModal.propTypes = {
  postId: PropTypes.string.isRequired,
  isOpened: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  del: PropTypes.func.isRequired,
  edit: PropTypes.func.isRequired
};

export default PostMenuModal;
