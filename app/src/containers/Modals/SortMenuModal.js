import React from 'react';
import { Button } from 'native-base';
import { Text } from 'react-native';
import Modal from '../../components/Modal';
import PropTypes from 'prop-types';
import globalStyles from '../../styles';

const SortMenuModal = ({ postId, isOpened, close, byDate, byLikes, byDislikes, byComments }) => (
  <Modal isOpened={isOpened} close={close} header='Sort posts'>
    <Button full transparent style={globalStyles.justifyFlexStart} onPress={byDate}>
      <Text style={globalStyles.fs18}>
        By date
      </Text>
    </Button>
    <Button full transparent style={globalStyles.justifyFlexStart} onPress={byLikes}>
      <Text style={globalStyles.fs18}>
        By likes
      </Text>
    </Button>
    <Button full transparent style={globalStyles.justifyFlexStart} onPress={byDislikes}>
      <Text style={globalStyles.fs18}>
        By dislikes
      </Text>
    </Button>
    <Button full transparent style={globalStyles.justifyFlexStart} onPress={byComments}>
      <Text style={globalStyles.fs18}>
        By comments
      </Text>
    </Button>
  </Modal>
);

SortMenuModal.defaultProps = {
  postId: ''
};

SortMenuModal.propTypes = {
  postId: PropTypes.string.isRequired,
  isOpened: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  byDate: PropTypes.func,
  byLikes: PropTypes.func,
  byDislikes: PropTypes.func,
  byComments: PropTypes.func
};

export default SortMenuModal;
