import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 30
  },
  profileImage: {
    height: 200,
    width: 200,
    borderRadius: 100,
    marginBottom: 20
  },
  profileText: {
    fontSize: 18
  }
});
