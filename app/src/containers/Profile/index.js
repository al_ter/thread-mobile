import React from 'react';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';
import { Image, View } from 'react-native';
import { Container, Text, Header, Body, Left, Right, Title, Button, Icon, Content } from 'native-base';
import styles from './styles';
import { createAlert } from '../../helpers/common';

const Profile = ({ user, currentUserId, logout }) => {
  const { id, username, image, email } = user;
  const navigation = useNavigation();

  const imgLink = image && image.link
    ? image.link
    : `https://i.pravatar.cc/500?u=${user.id}`;

  const handleLogOut = () => {
    createAlert(
      'Are you sure?',
      'Do you want to log out?',
      logout
    );
  };

  return (
    <Container>
      <Header>
        <Left>
          <Button transparent onPress={() => navigation.goBack()}>
            <Icon name='arrow-back' />
          </Button>
        </Left>
        <Body>
          <Title>{username}</Title>
        </Body>
        <Right>
          {
            currentUserId === id && (
              <Button transparent onPress={handleLogOut}>
                <Icon name='log-out-outline' />
              </Button>
            )
          }
        </Right>
      </Header>
      <Content padder>
        <View style={styles.profileContainer}>
          <Image source={{ uri: imgLink }} style={styles.profileImage} />
          {
            email && <Text style={styles.profileText}>Email: {email}</Text>
          }
        </View>
      </Content>
    </Container>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  currentUserId: PropTypes.string,
  logout: PropTypes.func
};

Profile.defaultProps = {
  user: {}
};

export default Profile;
