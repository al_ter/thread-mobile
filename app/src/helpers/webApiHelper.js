import * as queryString from 'query-string';
import { API_SERVER_IP, API_SERVER_PORT } from '@env';
import Storage from './asyncStorageHelper';

function getFetchUrl(args) {
  return `http://${API_SERVER_IP}:${API_SERVER_PORT}` + args.endpoint + (args.query ? `?${queryString.stringify(args.query)}` : '');
};

const getFetchArgs = async args => {
  const headers = {};
  if (!args.attachment) {
    headers['Content-Type'] = 'application/json';
    headers.Accept = 'application/json';
  }

  const token = await Storage.get('token');

  if (token && !args.skipAuthorization) {
    headers.Authorization = `Bearer ${token}`;
  }

  let body;

  if (args.attachment) {
    if (args.type === 'GET') {
      throw new Error('GET request does not support attachments.');
    }
    const formData = new FormData();
    formData.append('image', args.attachment);
    body = formData;
  } else if (args.request) {
    if (args.type === 'GET') {
      throw new Error('GET request does not support request body.');
    }
    body = JSON.stringify(args.request);
  }
  return {
    method: args.type,
    headers,
    signal: args.ct,
    ...(args.request === 'GET' ? {} : { body })
  };
};

export async function throwIfResponseFailed(res) {
  if (!res.ok) {
    let parsedException = 'Something went wrong with request!';
    try {
      parsedException = await res.json();
    } catch (err) {
      //
    }
    throw parsedException;
  }
};

export default async function callWebApi(args) {
  const reqArgs = await getFetchArgs(args);
  const res = await fetch(
    getFetchUrl(args),
    reqArgs
  );
  await throwIfResponseFailed(res);
  return res;
};
