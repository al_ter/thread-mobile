import AsyncStorage from '@react-native-async-storage/async-storage';

const AppNameSpace = 'ThreadApp:';

const set = async (key, value) => {
  try {
    await AsyncStorage.setItem(AppNameSpace + key, value);
  } catch (error) {
    console.log(error);
  }
};

const get = async key => {
  let value = null;
  try {
    value = await AsyncStorage.getItem(AppNameSpace + key);
  } catch (error) {
    console.log(error);
  }
  return value;
};

const Storage = {
  get,
  set
};

export default Storage;
