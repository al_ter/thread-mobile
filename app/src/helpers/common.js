import { Alert } from 'react-native';
import moment from 'moment';

export const findById = (entityId, array) => {
  return array.find(item => item.id === entityId);
};

export const findIdxById = (entityId, array) => {
  return array.findIndex(item => item.id === entityId);
};

export const createAlert = (title = '', message = '', onConfirm = () => {}, onCancel = () => {}) => (
  Alert.alert(
    title,
    message,
    [
      {
        text: 'Cancel',
        onPress: onCancel,
        style: 'cancel'
      },
      {
        text: 'Ok',
        onPress: onConfirm
      }
    ],
    { cancelable: false }
  )
);

export const sortByDate = (array, order) => {
  return order === 'asc'
    ? array.sort((a, b) => moment(a.createdAt).diff(b.createdAt))
    : array.sort((a, b) => moment(b.createdAt).diff(a.createdAt))
};

export const getUserImgLink = user => (user?.image && user.image.link
  ? user.image.link
  : `https://i.pravatar.cc/300?u=${user.id}`
);
