export const SIGN_IN = 'Thread/SignIn';
export const SIGN_UP = 'Thread/SignUp';
export const THREAD = 'Thread/Thread';
export const POST = 'Thread/Post';
export const PROFILE = 'Thread/Profile';
