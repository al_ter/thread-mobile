import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Profile from '../containers/Profile';
import { logout } from '../containers/Profile/actions';

const ProfileScreen = ({ user: self, logout: signOut, route }) => {
  let other;

  if (route.params?.user && route.params?.user.id !== self.id) {
    other = route.params.user;
  }

  return (
    <Profile user={other || self} currentUserId={self.id} logout={signOut} />
  );
};

ProfileScreen.propTypes = {
  user: PropTypes.objectOf(PropTypes.any)
};

Profile.defaultProps = {
  user: {}
};

const actions = { logout };

const mapStateToProps = state => {
  const { profile: user } = state;
  return user;
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
