import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { register } from '../containers/Profile/actions';
import { View, Image } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import { SIGN_IN } from '../routes';
import SignUpForm from '../components/SignUpForm';
import globalStyles from '../styles';

const logo = require('../assets/w256h2561377930292cattied.png');

const SignUpScreen = ({ navigation, register: signUp }) => (
  <Container>
    <Content padder contentContainerStyle={globalStyles.container}>
      <Image source={logo} style={globalStyles.logo} />
      <SignUpForm register={signUp} />
      <View style={globalStyles.already}>
        <Text>
          Already with us?
        </Text>
        <Button transparent onPress={() => navigation.navigate(SIGN_IN)}>
          <Text>
            Sign in
          </Text>
        </Button>
      </View>
    </Content>
  </Container>
);

SignUpScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  }),
  register: PropTypes.func.isRequired
};

const actions = { register };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(SignUpScreen);
