import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Image } from 'react-native';
import { View, Container, Body, Header, Content, Title, Button, Text } from 'native-base';
import PropTypes from 'prop-types';
import { login } from '../containers/Profile/actions';
import { SIGN_UP } from '../routes';
import SignInForm from '../components/SignInForm';
import globalStyles from '../styles';

const logo = require('../assets/w256h2561377930292cattied.png');

const SignInScreen = ({ navigation, login: signIn }) => (
  <Container>
    <Content padder contentContainerStyle={globalStyles.container}>
      <Image source={logo} style={globalStyles.logo} />
      <SignInForm login={signIn} />
      <View style={globalStyles.already}>
        <Text>
          New to us?
        </Text>
        <Button transparent onPress={() => navigation.navigate(SIGN_UP)}>
          <Text>
            Sign up here
          </Text>
        </Button>
      </View>
    </Content>
  </Container>
);

SignInScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  }),
  login: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch);

export default connect(null, mapDispatchToProps)(SignInScreen);
