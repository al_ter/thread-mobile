import React from 'react';
import Thread from '../containers/Thread';

const ThreadScreen = () => {
  return (
    <Thread />
  );
}

export default ThreadScreen;
