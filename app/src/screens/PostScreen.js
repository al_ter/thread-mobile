import React from 'react';
import ExpandedPost from '../containers/ExpandedPost';

const PostScreen = () => (
  <ExpandedPost />
);

export default PostScreen;
