import React from 'react';
import CustomCard from '../CustomCard';

const Post = props => (
  <CustomCard type='post' {...props} />
);

export default Post;
