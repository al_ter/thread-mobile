import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Item, Input, Button, Text } from 'native-base';
import globalStyles from '../../styles';

const SignInForm = ({ login }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleLoginClick = async () => {
    const isValid = isEmailValid && isPasswordValid;
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await login({ email, password });
    } catch {
      // TODO: show error
      setIsLoading(false);
    }
  };

  return (
    <Form style={globalStyles.form}>
      <Item>
        <Input
          placeholder="Email"
          onChangeText={emailChanged}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
      </Item>
      <Item>
        <Input
          placeholder="Password"
          onChangeText={passwordChanged}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
      </Item>
      <Button
        style={globalStyles.loginButton}
        block
        success
        onPress={handleLoginClick} disabled={isLoading}
      >
        <Text>
          Sign in
        </Text>
      </Button>
    </Form>
  );
}

SignInForm.propTypes = {
  login: PropTypes.func.isRequired
};

export default SignInForm;
