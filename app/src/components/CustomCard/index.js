import React from 'react';
import { Image } from 'react-native';
import { Card, Text, CardItem, Left, Right, Icon, Body, Button } from 'native-base';
import CustomCardHeader from './CustomCardHeader';
import CustomCardFooter from './CustomCardFooter';
import styles from './styles';

const CustomCard = ({
  type,
  entity,
  currentUserId,
  setReaction,
  editEntity,
  deleteEntity,
  openMenu,
  toggleExpandedPost
}) => {
  const {
    id,
    image,
    body,
    userId,
    createdAt,
    updatedAt,
    user,
    likeCount,
    dislikeCount,
    commentCount
  } = entity;

  return (
    <Card style={type === 'post' ? styles.post : styles.comment}>
      <CustomCardHeader
        user={user}
        currentUserId={currentUserId}
        createdAt={createdAt}
        updatedAt={updatedAt}
        openMenu={() => openMenu(entity)}
      />

      {
        image &&
        <CardItem cardBody>
          <Image source={{ uri: image.link }} style={styles.cardImage} />
        </CardItem>
      }

      <CardItem button onPress={() => toggleExpandedPost(id)}>
        <Body>
          <Text>{body}</Text>
        </Body>
      </CardItem>

      <CustomCardFooter
        entityId={id}
        user={user}
        currentUserId={currentUserId}
        likeCount={likeCount}
        dislikeCount={dislikeCount}
        commentCount={commentCount}
        setReaction={setReaction}
        toggleExpandedPost={toggleExpandedPost}
      />
    </Card>
  );
};

export default CustomCard;
