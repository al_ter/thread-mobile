import React from 'react';
import { Text, CardItem, Left, Right, Icon, Body, Button } from 'native-base';
import styles from './styles';

const CustomCardFooter = ({
  entityId,
  user,
  currentUserId,
  likeCount,
  dislikeCount,
  commentCount,
  setReaction,
  toggleExpandedPost
}) => {

  const likeIcon = likeCount > 0 ? 'thumbs-up' : 'thumbs-up-outline';
  const dislikeIcon = dislikeCount > 0 ? 'thumbs-down' : 'thumbs-down-outline';

  return (
    <CardItem footer>
      <Left>
        <Button transparent onPress={() => setReaction(entityId, true)}>
          <Icon style={styles.like} name={likeIcon} />
          <Text style={[styles.reactionCount, styles.like]}>{likeCount > 0 && likeCount}</Text>
        </Button>
        <Button transparent onPress={() => setReaction(entityId, false)}>
          <Icon style={styles.dislike} name={dislikeIcon} />
          <Text style={[styles.reactionCount, styles.dislike]}>{dislikeCount > 0 && dislikeCount}</Text>
        </Button>
      </Left>
      <Right>
        {
          commentCount > 0 &&
          <Button transparent onPress={() => toggleExpandedPost(entityId)}>
            <Text>{commentCount}</Text>
            <Icon name='chatbubbles-outline' />
          </Button>
        }
      </Right>
    </CardItem>
  );
};

export default CustomCardFooter;
