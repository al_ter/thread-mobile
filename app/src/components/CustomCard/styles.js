import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
  cardImage: {
    height: 200,
    width: null,
    flex: 1
  },
  cardMenu: {
    color: '#333'
  },
  post: {

  },
  comment: {
    paddingLeft: 20,
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    margin: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    elevation: 0
  },
  date: {
    minWidth: 200
  },
  reactionCount: {
    paddingLeft: 3
  },
  like: {
    color: '#23c95f'
  },
  dislike: {
    color: '#ff745a'
  }
});
