import React, { useCallback, useMemo } from 'react';
import { useNavigation } from '@react-navigation/native';
import { PROFILE } from '../../routes';
import moment from 'moment';
import { Text, CardItem, Left, Right, Thumbnail, Icon, Body, Button } from 'native-base';
import { getUserImgLink } from '../../helpers/common';
import styles from './styles';

const CustomCardHeader = ({
  user,
  currentUserId,
  createdAt,
  updatedAt,
  openMenu
}) => {
  const dateCreated = moment(createdAt).fromNow();
  const dateUpdated = moment(updatedAt).fromNow();
  const navigation = useNavigation();

  const imgLink = useMemo(() => getUserImgLink(user), [user]);

  const date = useMemo(() => (
    moment(updatedAt).isAfter(createdAt)
      ? `edited ${dateUpdated}`
      : dateCreated
  ), [createdAt, updatedAt]);

  const handleProfileLink = useCallback(() => navigation.navigate(PROFILE, { user }), [user]);

  return (
    <CardItem header bordered>
      <Left>
        <Button transparent onPress={handleProfileLink}>
          <Thumbnail small source={{ uri: imgLink }} />
        </Button>
        <Body>
          <Text>{`@${user.username}`}</Text>
          <Text note style={styles.date}>
            {date}
          </Text>
        </Body>
      </Left>
      <Right>
        {
          user.id === currentUserId && (
            <Button transparent onPress={openMenu}>
              <Icon name='ellipsis-vertical' style={styles.cardMenu} />
            </Button>
          )
        }
      </Right>
    </CardItem>
  );
};

export default CustomCardHeader;
