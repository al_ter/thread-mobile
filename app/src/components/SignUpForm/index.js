import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Item, Input, Button, Text } from 'native-base';
import globalStyles from '../../styles';

const SignUpForm = ({ register }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isUsernameValid, setIsUsernameValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const emailChanged = value => {
    setEmail(value);
    setIsEmailValid(true);
  };

  const usernameChanged = value => {
    setUsername(value);
    setIsUsernameValid(true);
  };

  const passwordChanged = value => {
    setPassword(value);
    setIsPasswordValid(true);
  };

  const handleSignUpClick = async () => {
    const isValid = isEmailValid && isUsernameValid && isPasswordValid;
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await register({ email, password, username });
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  return (
    <Form style={globalStyles.form}>
      <Item>
        <Input
          placeholder="Email"
          onChangeText={emailChanged}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
      </Item>
      <Item >
        <Input
          placeholder="Username"
          onChangeText={usernameChanged}
          onBlur={() => setIsUsernameValid(Boolean(username))}
        />
      </Item>
      <Item>
        <Input
          placeholder="Password"
          onChangeText={passwordChanged}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
      </Item>
      <Button
        style={globalStyles.loginButton}
        block
        success
        onPress={handleSignUpClick}
        disabled={isLoading}
      >
        <Text>
          Sign up
        </Text>
      </Button>
    </Form>
  );
}

SignUpForm.propTypes = {
  register: PropTypes.func.isRequired
};

export default SignUpForm;
