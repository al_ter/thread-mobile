import React from 'react';
import CustomCard from '../CustomCard';

const Comment = props => (
  <CustomCard type='comment' {...props} />
);

export default Comment;
