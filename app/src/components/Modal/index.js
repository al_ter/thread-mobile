import React from 'react';
import { Icon, Button, H3 } from 'native-base';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import NativeModal from 'react-native-modal';
import styles from './styles';

const Modal = ({ isOpened, close, header = '', children }) => (
  <NativeModal
    isVisible={isOpened}
    onBackButtonPress={close}
    onBackdropPress={close}
    onSwipeComplete={close}
    swipeDirection='down'
    hideModalContentWhileAnimating={true}
    avoidKeyboard={true}
    style={styles.modalContainer}
  >
    <View style={styles.modalContent}>
      <View style={styles.modalHeader}>
        <H3>{header}</H3>
        <Button transparent onPress={close}>
          <Icon name='close' />
        </Button>
      </View>
      <View>
        {children}
      </View>
    </View>
  </NativeModal>
);

Modal.defaultProps = {
  isOpened: false,
  header: ''
};

Modal.propTypes = {
  isOpened: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  header: PropTypes.string
};

export default Modal;
