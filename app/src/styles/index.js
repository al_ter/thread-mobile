import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly'
  },
  logo: {
    alignSelf: 'center',
    height: 100,
    width: 100
  },
  already: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  loginButton: {
    marginTop: 20
  },
  form: {
    paddingLeft: 0,
    marginLeft: 0
  },
  commentButton: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  fs16: {
    fontSize: 16
  },
  fs18: {
    fontSize: 18
  },
  colorRed: {
    color: 'red'
  },
  profileLink: {
    color: '#3880ff',
    fontWeight: '600'
  },
  alignCenter: {
    alignItems: 'center'
  },
  justifyFlexStart: {
    justifyContent: 'flex-start'
  },
  formFooter: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginTop: 20
  },
  formSendButtonText: {
    marginHorizontal: 15,
    fontSize: 18,
    color: '#ffffff',
    fontWeight: '600'
  },
  mr10: {
    marginRight: 10
  }
});
