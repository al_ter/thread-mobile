import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavigationContainer } from '@react-navigation/native';
import AuthStack from './AuthNavigator';
import AppStack from './AppNavigator';
import SplashScreen from '../containers/SplashScreen';
import { loadCurrentUser } from '../containers/Profile/actions';

const RootNavigator = ({
  user,
  isAuthorized,
  loadCurrentUser: loadUser,
  isLoading
}) => {
  useEffect(() => {
    if (!isAuthorized) {
      loadUser();
    }
  });

  return (
    isLoading
      ? <SplashScreen />
      : (
        <NavigationContainer>
          {
            isAuthorized
              ? <AppStack />
              : <AuthStack />
          }
        </NavigationContainer>
      )
  );
};

RootNavigator.propTypes = {
  isAuthorized: PropTypes.bool,
  user: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  loadCurrentUser: PropTypes.func.isRequired
};

RootNavigator.defaultProps = {
  isAuthorized: false,
  user: {},
  isLoading: true
};

const actions = { loadCurrentUser };

const mapStateToProps = ({ profile }) => ({
  isAuthorized: profile.isAuthorized,
  user: profile.user,
  isLoading: profile.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RootNavigator);
