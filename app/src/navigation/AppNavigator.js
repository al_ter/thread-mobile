import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { THREAD, POST, PROFILE } from '../routes';
import ThreadScreen from '../screens/ThreadScreen';
import PostScreen from '../screens/PostScreen';
import ProfileScreen from '../screens/ProfileScreen';

const AppStack = createStackNavigator();

const AppNavigator = () => {
  return (
    <AppStack.Navigator screenOptions={{ headerShown: false }}>
      <AppStack.Screen name={THREAD} component={ThreadScreen} />
      <AppStack.Screen name={POST} component={PostScreen} />
      <AppStack.Screen name={PROFILE} component={ProfileScreen} />
    </AppStack.Navigator>
  );
};

export default AppNavigator;
