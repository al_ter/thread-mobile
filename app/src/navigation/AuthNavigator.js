import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { SIGN_IN, SIGN_UP } from '../routes';
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';

const AuthStack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <AuthStack.Navigator screenOptions={{ headerShown: false }}>
      <AuthStack.Screen name={SIGN_IN} component={SignInScreen} />
      <AuthStack.Screen name={SIGN_UP} component={SignUpScreen} />
    </AuthStack.Navigator>
  );
};

export default AuthNavigator;
