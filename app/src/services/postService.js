import callWebApi from '../helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request
  });
  return response.json();
};

export const updatePost = async post => {
  const response = await callWebApi({
    endpoint: `/api/posts/${post.id}`,
    type: 'PUT',
    request: post
  });
  return response.json();
};

export const deletePost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'DELETE'
  });
  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const setPostReaction = async (postId, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/posts/react',
    type: 'PUT',
    request: {
      postId,
      isLike
    }
  });
  return response.json();
};

export const getPostReactions = async postId => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}/react`,
    type: 'GET'
  });
  return response.json();
};

// should be replaced by appropriate function
export const getPostByHash = async hash => getPost(hash);
