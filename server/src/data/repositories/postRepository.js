import { Op } from 'sequelize';
import sequelize from '../db/connection';

import {
  PostModel,
  CommentModel,
  CommentReactionModel,
  UserModel,
  ImageModel,
  PostReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likePostCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      not,
      column = 'createdAt',
      desc = 1
    } = filter;

    const where = {};
    if (userId) {
      if (not) {
        Object.assign(where, { userId: { [Op.ne]: userId } });
      } else {
        Object.assign(where, { userId });
      }
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likePostCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likePostCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [
        [sequelize.col(column), parseInt(desc, 10) ? 'DESC' : 'ASC']
      ],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likePostCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likePostCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "commentReactions" as "commentReaction"
                        WHERE "comments"."id" = "commentReaction"."commentId"
                        AND "commentReaction"."isLike" = TRUE)`), 'likeCount'],
            [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "commentReactions" as "commentReaction"
                        WHERE "comments"."id" = "commentReaction"."commentId"
                        AND "commentReaction"."isLike" = FALSE)`), 'dislikeCount']
          ]
        },
        include: [{
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: CommentReactionModel,
          attributes: []
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
