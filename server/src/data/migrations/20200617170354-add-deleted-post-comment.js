export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('comments', 'deletedAt', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('posts', 'deletedAt', {
        type: Sequelize.DATE
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('comments', 'deletedAt', { transaction }),
      queryInterface.removeColumn('posts', 'deletedAt', { transaction })
    ]))
};
