import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .get('/:id/react', (req, res, next) => postService.getPostReactions(req.params.id)
    .then(reactions => res.send(reactions))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setPostReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        if (req.body.isLike === true) {
          req.io.to(reaction.post.userId).emit('like', req.user, 'post');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', req.user, 'post');
        }
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.update(req.body)
    .then(post => res.send(post))
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePost(req.params.id)
    .then(post => res.send(post))
    .catch(next));

export default router;
