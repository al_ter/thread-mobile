import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const update = comment => commentRepository.updateById(comment.id, comment);

export const getCommentById = id => commentRepository.getCommentById(id);

// export const deleteComment = id => commentRepository.deleteById(id);

export const deleteComment = async id => {
  const comment = await commentRepository.getCommentById(id);
  await commentRepository.deleteById(id);
  return comment;
};

export const setCommentReaction = async (userId, { commentId, isLike }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : commentReactionRepository.getCommentReaction(userId, commentId);
};

export const getCommentReactions = async commentId => {
  const result = await commentReactionRepository.getCommentReactions(commentId);
  return result;
};
