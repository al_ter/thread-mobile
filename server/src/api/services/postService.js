import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const deletePost = async id => {
  const post = await postRepository.getPostById(id);
  await postRepository.deleteById(id);
  return post;
};

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const update = post => postRepository.updateById(post.id, post);

export const setPostReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const getPostReactions = async postId => {
  const result = await postReactionRepository.getPostReactions(postId);
  return result;
};
